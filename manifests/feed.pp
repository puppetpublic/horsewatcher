# The client interface to horsewatcher.  Defines a feed of the provided name
# type to the central horsewatcher server.

define horsewatcher::feed(
    $ensure = 'present'
) {
    include horsewatcher

    base::daemontools::supervise { "horsewatcher-$name":
        ensure  => $ensure,
        content => template('horsewatcher/horsewatcher-feed-run.erb'),
    }
}