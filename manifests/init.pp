# Set up a system to feed data to horsewatcher.
#
# We have to be sure that syslog is mentioned on TCP as well as UDP because
# the Perl Sys::Syslog module refuses to attempt TCP unless syslog or syslogng
# is defined in /etc/services for TCP.

# Augeas doesn't appear to work on lenny and doesn't work on Red Hat, so only
# use the Augeas method for newer Debian and Ubuntu.
class horsewatcher {
    case $lsbdistcodename {
        'squeeze', 'wheezy', 'natty', 'oneiric': {
            augeas { 'services/syslogng/tcp':
                context => '/files/etc/services',
                changes => [
                    "ins service-name after #comment[last()]",
                    "set service-name[last()] syslogng",
                    "set service-name[last()]/port 514",
                    "set service-name[last()]/protocol tcp"
                ],
                onlyif  => "match service-name[. = 'syslogng'] size == 0",
            }
        }
        default: {
            base::textline { 'syslogng 514/tcp':
                ensure => '/etc/services',
            }
        }
    }
}